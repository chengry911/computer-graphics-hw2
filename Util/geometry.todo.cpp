#include <stdlib.h>
#include <math.h>

#include <SVD/SVDFit.h>
#include <SVD/MatrixMNTC.h>

#include "geometry.h"


///////////////////////
// Ray-tracing stuff //
///////////////////////
double BoundingBox3D::intersect(const Ray3D& ray) const {
     double  xmax, xmin, ymin, ymax, zmin, zmax;
     Point3D P = ray.position;
     Point3D d = ray.direction;

	 /**If ray starts inside of box, return 0*/
	 if(P.p[0]<=p[1].p[0]&&p[0].p[0]<=P.p[0]
		&&P.p[1]<=p[1].p[1]&&p[0].p[1]<=P.p[1]
		&&P.p[2]<=p[1].p[2]&&p[0].p[2]<=P.p[2]){
	 	return 0;
	 }
     if(d.p[0]>=0){
        xmin =(p[0].p[0]-P.p[0])/d.p[0];
        xmax =(p[1].p[0]-P.p[0])/d.p[0];
     }else{
        xmin = (p[1].p[0] - P.p[0])/d.p[0];
        xmax = (p[0].p[0] - P.p[0])/d.p[0];
     }
     if(d.p[1]>=0){
        ymin = (p[0].p[1]-P.p[1])/d.p[1];
        ymax = (p[1].p[1]-P.p[1])/d.p[1];
     }else{
        ymin = (p[1].p[1] - P.p[1])/d.p[1];
        ymax = (p[0].p[1] - P.p[1])/d.p[1];
     }
     if(d.p[2]>=0){
        zmin = (p[0].p[2]-P.p[2])/d.p[2];
        zmax = (p[1].p[2]-P.p[2])/d.p[2];
     }else{
        zmin = (p[1].p[2]-P.p[2])/d.p[2];
        zmax = (p[0].p[2]-P.p[2])/d.p[2];
     }
	 if((xmin > ymax)||(ymin > xmax)||(xmin > zmax)||(zmin > xmax)){
        return -1;
     }
     if(ymin > xmin){
        xmin = ymin;
     }
     if(zmin > xmin){
        xmin = zmin;
     }

     return xmin;
}

/////////////////////
// Animation stuff //
/////////////////////
Matrix3D::Matrix3D(const Point3D& e){
	(*this)=Matrix3D();
}

Matrix3D::Matrix3D(const Quaternion& q){
	(*this)=Matrix3D();
}
Matrix3D Matrix3D::closestRotation(void) const {
	return (*this);
}
/* While these Exp and Log implementations are the direct implementations of the Taylor series, the Log
 * function tends to run into convergence issues so we use the other ones:*/
Matrix3D Matrix3D::Exp(const Matrix3D& m,int iter){
	return m;
}
