#include "image.h"
#include <stdlib.h>
#include <math.h>

////////////////////////////
// Image processing stuff //
////////////////////////////
Pixel::Pixel(const Pixel32& p)
{
}
Pixel32::Pixel32(const Pixel& p)
{
}

int Image32::AddRandomNoise(const float& noise,Image32& outputImage) const
{
	return 0;
}
int Image32::Brighten(const float& brightness,Image32& outputImage) const
{
	return 0;
}

int Image32::Luminance(Image32& outputImage) const
{
	return 0;
}

int Image32::Contrast(const float& contrast,Image32& outputImage) const
{
	return 0;
}

int Image32::Saturate(const float& saturation,Image32& outputImage) const
{
	return 0;
}

int Image32::Quantize(const int& bits,Image32& outputImage) const
{
	return 0;
}

int Image32::RandomDither(const int& bits,Image32& outputImage) const
{
	return 0;
}
int Image32::OrderedDither2X2(const int& bits,Image32& outputImage) const
{
	return 0;
}

int Image32::FloydSteinbergDither(const int& bits,Image32& outputImage) const
{
	return 0;
}

int Image32::Blur3X3(Image32& outputImage) const
{
	outputImage.setSize(w,h);
	printf("Height: %d Width: %d.\n", outputImage.height(), outputImage.width());
	
	const int DSize = 3;

	int filter[DSize][DSize] = {{1, 2, 1}, 
								{2, 4, 2}, 
								{1, 2, 1}}; 

		for (int y = 0; y < outputImage.height(); y++)
		{
			for (int x = 0; x < outputImage.width(); x++)
			{	
				Pixel32& p1 = outputImage.pixel(x, y);

				if(y==0 || y== outputImage.height()-1){
					p1.r = pixels[ y*w + x].r;
				    p1.g = pixels[ y*w + x].g;
				    p1.b = pixels[ y*w + x].b;
				}else if(x==0 || x == outputImage.width()-1){
					p1.r = pixels[ y*w + x].r;
				    p1.g = pixels[ y*w + x].g;
				    p1.b = pixels[ y*w + x].b;				    
				}else{
				
				    //printf("%d ", filter[0][1]);
				    p1.r = pixels[ (y-1)*w + (x-1)].r*filter[0][0]/16 + pixels[ (y-1)*w + x].r*filter[0][1]/16
						+ pixels[ (y-1)*w + (x+1)].r*filter[0][2]/16 + pixels[ y*w + (x-1)].r*filter[1][0]/16
						+ pixels[ y*w + x].r*filter[1][1]/16 + pixels[ y*w + x+1 ].r*filter[1][2]/16 
						+ pixels[ (y+1)*w + (x-1)].r*filter[2][0]/16 + pixels[ (y+1)*w + x].r*filter[2][1]/16
						+ pixels[ (y+1)*w + (x+1)].r*filter[2][2]/16;
				    p1.g = pixels[ (y-1)*w + (x-1)].g*filter[0][0]/16 + pixels[ (y-1)*w + x].g*filter[0][1]/16
						+ pixels[ (y-1)*w + (x+1)].g*filter[0][2]/16 + pixels[ y*w + (x-1)].g*filter[1][0]/16
						+ pixels[ y*w + x].g*filter[1][1]/16 + pixels[ y*w + x+1 ].g*filter[1][2]/16 
						+ pixels[ (y+1)*w + (x-1)].g*filter[2][0]/16 + pixels[ (y+1)*w + x].g*filter[2][1]/16
						+ pixels[ (y+1)*w + (x+1)].g*filter[2][2]/16;
				    p1.b = pixels[ (y-1)*w + (x-1)].b*filter[0][0]/16 + pixels[ (y-1)*w + x].b*filter[0][1]/16
						+ pixels[ (y-1)*w + (x+1)].b*filter[0][2]/16 + pixels[ y*w + (x-1)].b*filter[1][0]/16
						+ pixels[ y*w + x].b*filter[1][1]/16 + pixels[ y*w + x+1 ].b*filter[1][2]/16 
						+ pixels[ (y+1)*w + (x-1)].b*filter[2][0]/16 + pixels[ (y+1)*w + x].b*filter[2][1]/16
						+ pixels[ (y+1)*w + (x+1)].b*filter[2][2]/16;		
					if(p1.r <0){
						p1.r = 0;
					}
					if(p1.g < 0){
						p1.g = 0;
					}
					if(p1.b < 0){
						p1.b = 0;
					}
					if(p1.r > 255){
						p1.r = 255;
					}
					if(p1.g > 255){
						p1.g = 255;
					}
					if(p1.b > 255){
						p1.b = 255;
					}
				}	
			}
		}
 
		return 1;
}

int Image32::EdgeDetect3X3(Image32& outputImage) const
{
	return 0;
}
int Image32::ScaleNearest(const float& scaleFactor,Image32& outputImage) const
{
	return 0;
}

int Image32::ScaleBilinear(const float& scaleFactor,Image32& outputImage) const
{
	return 0;
}

int Image32::ScaleGaussian(const float& scaleFactor,Image32& outputImage) const
{
	return 0;
}

int Image32::RotateNearest(const float& angle,Image32& outputImage) const
{
	return 0;
}

int Image32::RotateBilinear(const float& angle,Image32& outputImage) const
{
	return 0;
}
	
int Image32::RotateGaussian(const float& angle,Image32& outputImage) const
{
	return 0;
}


int Image32::SetAlpha(const Image32& matte)
{
	return 0;
}

int Image32::Composite(const Image32& overlay,Image32& outputImage) const
{
	return 0;
}

int Image32::CrossDissolve(const Image32& source,const Image32& destination,const float& blendWeight,Image32& ouputImage)
{
	return 0;
}
int Image32::Warp(const OrientedLineSegmentPairs& olsp,Image32& outputImage) const
{
	return 0;
}

int Image32::FunFilter(Image32& outputImage) const
{
	return 0;
}
int Image32::Crop(const int& x1,const int& y1,const int& x2,const int& y2,Image32& outputImage) const
{
	return 0;
}


Pixel32 Image32::NearestSample(const float& x,const float& y) const
{
	return Pixel32();
}
Pixel32 Image32::BilinearSample(const float& x,const float& y) const
{
	return Pixel32();
}
Pixel32 Image32::GaussianSample(const float& x,const float& y,const float& variance,const float& radius) const
{
	return Pixel32();
}
