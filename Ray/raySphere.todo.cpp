#include <math.h>
#include <GL/glut.h>
#include "rayScene.h"
#include "raySphere.h"

////////////////////////
//  Ray-tracing stuff //
////////////////////////
double min(double a, double b) {
        if (a < b)
                return a;
        return b;
}
double max(double a, double b) {
        if (a > b)
                return a;
        return b;
}

double RaySphere::intersect(Ray3D ray,RayIntersectionInfo& iInfo,double mx){
	Point3D ct = center;
	double r = radius;
	Point3D dir = ray.direction;
	Point3D posit = ray.position;

	/**This method implements the way described in the lecture Power Point*/
	double result = -1;
	double A, B, C, k;
	A = dir.dot(dir);
	B = 2*dir.dot(posit - ct);
	C = (posit - ct).dot(posit - ct) - r*r;
	k = B*B - 4*A*C;

	if(k < 0){
		/**There is no intersects.*/
		return -1;
	}
	double sqt = sqrt(k);
	double p1,p2,p3,p4;
	if(A==0){
		return -1;
	}else{
		p1 = (-B + sqt)/(2*A);
		p2 = (-B - sqt)/(2*A);
	}

	if(p1<0.0 && p2<0.0){
		return -1;
	}else{
		p3 = min(p1, p2);
		p4 = max(p1, p2);
		
		if(p3 < 0){

			Point3D icoo = ray(p4);
			Point3D norm = icoo - ct;
			norm /= (norm.length());
			iInfo.iCoordinate = icoo;
            iInfo.material = material;
            iInfo.normal = norm;
            iInfo.texCoordinate = Point2D();
			result = p4;
		}else{

			Point3D icoo = ray(p3);
			Point3D norm = icoo - ct;
			norm /= (norm.length());
			iInfo.iCoordinate = icoo;
            iInfo.material = material;
            iInfo.normal = norm;
            iInfo.texCoordinate = Point2D();
			result = p3;
		}
	}

	return result;
}
BoundingBox3D RaySphere::setBoundingBox(void){
	Point3D p;
	double rad = radius;
	p = Point3D(rad,rad,rad);
	/**Build a square box that has the diameter of the radius*/
	bBox=BoundingBox3D(center + p,center - p);
	return bBox;
}

//////////////////
// OpenGL stuff //
//////////////////
int RaySphere::drawOpenGL(int materialIndex){
	return -1;
}
