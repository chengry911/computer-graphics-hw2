#include <stdlib.h>
#include <GL/glut.h>
#include "rayGroup.h"

////////////////////////
//  Ray-tracing stuff //
////////////////////////
double RayGroup::intersect(Ray3D ray,RayIntersectionInfo& iInfo,double mx){
	double minDistance = mx;
	double tempDistance = 0.0;
	double bBoxIntersect = 0.0;
	int j=0;
	RayIntersectionInfo minInfo;
	Ray3D transRay = Ray3D();

	/**Get the transformed ray by getInverseMatrix()*/
	transRay.direction = getInverseMatrix().multDirection(ray.direction)/getInverseMatrix().multDirection(ray.direction).length();
	transRay.position = getInverseMatrix().multPosition(ray.position);

	/**Calculate all the bounding box intersections and put them into hits together with shapes*/
	for(int i=0; i<sNum; i++){
		bBoxIntersect = shapes[i]->bBox.intersect(transRay);
				
		if (bBoxIntersect > 0){
           hits[j].t = bBoxIntersect;
           hits[j].shape = shapes[i];
           j++;
        }
	}
	qsort(hits, j, sizeof(RayShapeHit), RayShapeHit::Compare);

	for(int i=0; i<j; i++){
		tempDistance = hits[i].shape->intersect(transRay, minInfo, minDistance);
		/**If the distance is greater than mx and mx is larger than 0, dismiss the calculation after*/
        if (hits[i].t > mx && mx > 0){
           break;
        }
		if(minDistance < 0 && tempDistance > 0){
			
			minDistance = tempDistance;
			minDistance = (minInfo.iCoordinate - transRay.position).length();
			/**Set the transformed coordinate information into the new one*/
			minInfo.iCoordinate = getNormalMatrix()*minInfo.iCoordinate;
			minInfo.normal = getNormalMatrix().multNormal(minInfo.normal);
			minInfo.normal /= minInfo.normal.length();

			iInfo = minInfo;
		}

		if(minDistance > 0 && minDistance > tempDistance&& tempDistance > 0){
			
			minDistance = tempDistance;
			minDistance = (minInfo.iCoordinate - transRay.position).length();
			/**Set the transformed coordinate information into the new one*/
			minInfo.iCoordinate = getNormalMatrix()*minInfo.iCoordinate;
			minInfo.normal = getNormalMatrix().multNormal(minInfo.normal);
			minInfo.normal /= minInfo.normal.length();

			iInfo = minInfo;
			
		}
		
	}

	if(minDistance==mx){
		return -1;
	}else{
		return minDistance;
	}
}

BoundingBox3D RayGroup::setBoundingBox(void){
	BoundingBox3D tempBox;
	Point3D* positions = new Point3D[sNum*2];
	/**Put bounding boxes into array*/
	for(int i=0; i<sNum; i++){
		tempBox = shapes[i]->setBoundingBox();
		for(int j=0; j<2; j++){
			positions[2*i+j]=tempBox.p[j];		
		}
	}
	tempBox=BoundingBox3D(positions,sNum*2);

	bBox=tempBox.transform(getMatrix());

	return bBox;
}

int StaticRayGroup::set(void){
	Matrix4D matrx = getMatrix();
	/**Set the normal and inverse transformation when initialized*/
	this->inverseTransform = matrx.invert();
	this->normalTransform = matrx;
	
	return 1;
}
//////////////////
// OpenGL stuff //
//////////////////
int RayGroup::getOpenGLCallList(void){
	return 0;
}

int RayGroup::drawOpenGL(int materialIndex){
	return -1;
}

/////////////////////
// Animation Stuff //
/////////////////////
Matrix4D ParametrizedEulerAnglesAndTranslation::getMatrix(void){
	return Matrix4D::IdentityMatrix();
}
Matrix4D ParametrizedClosestRotationAndTranslation::getMatrix(void){
	return Matrix4D::IdentityMatrix();
}
Matrix4D ParametrizedRotationLogarithmAndTranslation::getMatrix(void){
	return Matrix4D::IdentityMatrix();
}
Matrix4D ParametrizedQuaternionAndTranslation::getMatrix(void){
	return Matrix4D::IdentityMatrix();
}
