#include <math.h>
#include <GL/glut.h>
#include "rayDirectionalLight.h"
#include "rayScene.h"

////////////////////////
//  Ray-tracing stuff //
////////////////////////
double clamp(double input){
	if(input < 0){
		input = 0;
	}else if(input > 1){
		input = 1;
	}
	return input;
}

Point3D RayDirectionalLight::getDiffuse(Point3D cameraPosition,RayIntersectionInfo& iInfo){
        Point3D L = -direction;
		Point3D N = iInfo.normal;
        Point3D IL = color;
        Point3D KD = iInfo.material->diffuse;

        Point3D ID = KD*(IL*(N.dot(L)));
		/**Clamp to zero and one*/
		clamp(ID[0]);
		clamp(ID[1]);
		clamp(ID[2]);
		//printf("%lf %lf %lf", ID[0], ID[1], ID[2]);
        return ID;
}
Point3D RayDirectionalLight::getSpecular(Point3D cameraPosition,RayIntersectionInfo& iInfo){
		/**Calculate the coordinates*/
		Point3D L = -direction/direction.length();
        Point3D IL = color;
        Point3D KS = iInfo.material->specular;
		Point3D N = iInfo.normal/(iInfo.normal.length());
		if (direction.dot(N) > 0){
           return Point3D();
        }
        Point3D V = (cameraPosition - iInfo.iCoordinate);
				V /= V.length();
        Point3D R = direction - (N * 2 * (direction.dot(N)));
				R /= R.length();

        double n = iInfo.material->specularFallOff;

        Point3D IS = KS*IL*pow((V.dot(R)), n);
		/**Clamp to zero and one*/
		clamp(IS[0]);
		clamp(IS[1]);
		clamp(IS[2]);

		return IS;
}
int RayDirectionalLight::isInShadow(RayIntersectionInfo& iInfo,RayShape* shape,int& isectCount){
	    double interNum;
        Point3D d = -direction;
		Point3D P = iInfo.iCoordinate;
		RayIntersectionInfo tempiInfo = iInfo;
        Ray3D shadowBeam = Ray3D(P, d);

		/**Calculate the position of shadow beam, which is the inversed direction from the origion*/
        shadowBeam.position = shadowBeam(RAYEPS);

		/**Calculate the intesection nums*/
        interNum = shape->intersect(shadowBeam, tempiInfo, -1);
        Point3D N = tempiInfo.normal;

		if (d.dot(N)>= 0) {
             return 1;
        }
		/**If the shadow bean doesn't intersect with object, there's no shadow*/
		if (interNum > 0){
           return 0;		
		}
		return 1;
}
Point3D RayDirectionalLight::transparency(RayIntersectionInfo& iInfo,RayShape* shape,Point3D cLimit){
	
		double dist = 0.0;
		Point3D key;
        RayIntersectionInfo tempInfo = iInfo;
		Point3D tempLimit = cLimit;
        Point3D transparency=Point3D(1, 1, 1);
		Ray3D shadow = Ray3D(iInfo.iCoordinate, -direction);
		shadow.position = shadow(RAYEPS);
        
		while ((key[0] < tempLimit[0]) && (key[1] < tempLimit[1]) && (key[2] < tempLimit[2])) {
           dist = shape->intersect(shadow, tempInfo, -1);
		   /**If ray doesn't intersect with shape or the side faces to camera, return default*/
		   if (dist == -1 || (-direction).dot(tempInfo.normal) >= 0) {
               return transparency;
           }
           key = tempInfo.material->transparent;
		   transparency = transparency - key;
           tempLimit /= key;
        }

		clamp(transparency[0]);
		clamp(transparency[1]);
		clamp(transparency[2]);

        return transparency;

}

//////////////////
// OpenGL stuff //
//////////////////
void RayDirectionalLight::drawOpenGL(int index){
}