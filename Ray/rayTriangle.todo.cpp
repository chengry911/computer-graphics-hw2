#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "rayTriangle.h"

////////////////////////
//  Ray-tracing stuff //
////////////////////////
double d;
Point3D norm;
void RayTriangle::initialize(void){
	/**Initialize the vertex positions*/
	    Point3D V0 = v[0]->position;
        Point3D V1 = v[1]->position;
        Point3D V2 = v[2]->position;
		norm = plane.normal;
        d = plane.distance;
}
double RayTriangle::intersect(Ray3D ray,RayIntersectionInfo& iInfo,double mx){
	
        Point3D v0, v1, v2;
		v0 = v[0]->position;
        v1 = v[1]->position;
		v2 = v[2]->position;
		
        Point3D posit = ray.position;
        Point3D dir = ray.direction;

		/**This implements the methods described in this paper:**/
		/**Fast Minimum Storage Ray Triangle Intersection**/

        Point3D E1 = v1 - v0;
        Point3D E2 = v2 - v0;
		Point3D P = dir.crossProduct(E2);
		double det = E1.dot(P);
		Point3D T;
		if(det > 0){
			T = posit - v0;
		}else{
			T = v0 - posit;
			det = -det;
		}
		/**If determinate is near zero, then the ray lies in the plane of triangle*/
		if(det < 0.000000001){
			return -1;
		}
		double u = T.dot(P);
		if(u < 0 || u > det){			
			return -1;
		}
		Point3D Q = T.crossProduct(E1);
		double v = dir.dot(Q);
		if(v < 0 || u + v > det){			
			return -1;
		}
		double t = E2.dot(Q);

		det = 1.0/det;
		u *= det;
		v *= det;
		t *= det;
		/**Apply iInfo variables*/
        iInfo.iCoordinate = ray(t);
        iInfo.material = material;		
        iInfo.normal = (this->v[0]->normal*(1-u-v))+(this->v[1]->normal*u)+(this->v[2]->normal*v);
        iInfo.texCoordinate = Point2D();

        return t;

}
BoundingBox3D RayTriangle::setBoundingBox(void){
	Point3D position[3];
	/**Get the vertex position*/
	position[0]=v[0]->position;
	position[1]=v[1]->position;
	position[2]=v[2]->position;

	bBox=BoundingBox3D(position,3);
	/**Add roundings*/
	for(int j=0; j<2; j++){
		for(int i=0;i<3;i++){
			bBox.p[j][i]-=RAYEPS;
			bBox.p[j][i]+=RAYEPS;
		}
	}

	return bBox;
}

//////////////////
// OpenGL stuff //
//////////////////
int RayTriangle::drawOpenGL(int materialIndex){
	return -1;
}
