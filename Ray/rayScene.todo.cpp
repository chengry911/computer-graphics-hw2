#include "rayScene.h"
#include <GL/glut.h>
#include <math.h>


///////////////////////
// Ray-tracing stuff //
///////////////////////

Point3D RayScene::Reflect(Point3D v,Point3D n){
	Point3D reflect;
	/**Calculate the reflected direction*/
	double tempRefl = -v.dot(n);
	reflect = v + n*2*tempRefl;
	return reflect;
}

int RayScene::Refract(Point3D v,Point3D n,double ir,Point3D& refract){

	/**This implementation is from Bram de Greve's paper: "Reflections and Refractions in Ray Tracing"*/
	double cos1 = (-n).dot(v);
	double isAllRefra = 1-(1/ir*ir)*(1-cos1*cos1);
	if(isAllRefra > 0){
		double cos2 = sqrt(1-(1/ir*ir)*(1-cos1*cos1));
		Point3D t1 = (v + n*cos1)*(1/ir);
		Point3D t2 = -n*cos2;
		refract = t1 + t2;
		return 1;
	}else{
		return 0;
	}
	 
    return 1;
}

Ray3D RayScene::GetRay(RayCamera* camera,int i,int j,int width,int height){
	Ray3D ray;
	double d = 1.0;
	Point3D p0, p1, p2, p3, pij;
	double theta = camera -> heightAngle;
	theta /=2;
	/**Get the camera directions and position*/
	p0 = camera ->position;
	Point3D up = camera->up;
	Point3D right = camera->right;
	Point3D towards = camera->direction;
	/**Get the two coordinates */
	p1 = p0 + towards*d - up*d*tan(theta) - right*d*tan(theta);
	p2 = p0 + towards*d - up*d*tan(theta) + right*d*tan(theta);
	p3 = p0 + towards*d + up*d*tan(theta) - right*d*tan(theta);
	/**Get the target ray destination*/
	pij = p1 + (p2-p1)*((i+0.5)/width) + (p3-p1)*((j+0.5)/height);

	Point3D dir = pij - p0;
	/**Cast the ray to normal*/
	ray.direction = dir/dir.length();
	ray.position = p0;

	return ray;
}

Point3D RayScene::GetColor(Ray3D ray,int rDepth,Point3D cLimit){
	
	Point3D resultColor, ts;
	int recurDepth = rDepth;
	double distance = 0.0;
	double isRefractable = 0.0;
	RayIntersectionInfo iInfo;

	Point3D unitRayDir = ray.direction/ray.direction.length();
	Point3D unitNormal = iInfo.normal/iInfo.normal.length();

	/**Calculate the distances*/
	distance = group->intersect(ray, iInfo, -1);
	int shadow = 0;
	int itersecNum = 0;
	/**If there is no intersect on the object, apply background*/
	if(distance <= 0){
		resultColor = background;

	}else{
		Point3D reflected;
		/**Calculate the reflected direction*/
		if(ray.direction.dot(iInfo.normal)<0){
			reflected = Reflect(ray.direction, iInfo.normal);
			reflected /= reflected.length();
		}else{
			reflected = Reflect(ray.direction, iInfo.normal*(-1));
			reflected /= reflected.length();
		}
		/**Calculate the reflected ray*/
		Ray3D reflecRay = Ray3D(iInfo.iCoordinate, reflected);
		
		reflecRay.position = reflecRay(RAYEPS);
		/**If specular and diffuse light have tiny contributions, neglect*/
		if( ((iInfo.material->specular[0] <= cLimit[0])&& (iInfo.material->specular[1] <=cLimit[1])
			&& (iInfo.material->specular[2]<= cLimit[2]))||((iInfo.material->diffuse[0] <= cLimit[0])
			&& (iInfo.material->diffuse[1] <=cLimit[1]) && (iInfo.material->diffuse[2]<= cLimit[2]))){
			return resultColor;
		}

		if(rDepth > 0){
			/**Recursionly get the reflected colors*/
			Point3D reflectedColor = GetColor(reflecRay, rDepth - 1,(cLimit / iInfo.material->specular))*iInfo.material->specular;
			resultColor += reflectedColor;
			/**Add ambient and emissive */
			resultColor += ambient*iInfo.material->ambient + iInfo.material->emissive;

			/**Add diffuse*/
			for(int j=0; j<this->lightNum; j++){
				shadow = this->lights[j]->isInShadow(iInfo, this->group, itersecNum);
				ts = lights[j]->transparency(iInfo, this->group, cLimit);
				resultColor += ((this->lights[j]->getDiffuse(ray.position, iInfo) + this->lights[j]->getSpecular(ray.position, iInfo)))*ts;//*shadow;			
			}

			/**Work on the refractions*/
			Ray3D refractRay;
			isRefractable = this->Refract(ray.direction, iInfo.normal, iInfo.material->refind, refractRay.direction);
			if(isRefractable){
				refractRay.position = iInfo.iCoordinate;
				refractRay.position = refractRay(RAYEPS);
				/**Recursionly get the refract colors*/
				if(ts.p[0]>=cLimit[0]&&ts.p[1]>=cLimit[1]&&ts.p[2]>=cLimit[2]){
					Point3D refractColor = this->GetColor(refractRay, rDepth-1, cLimit);
				
				/**If the refracted color to the end, stop*/
				for(int i=0; i<3; i++){
					if(refractColor.p[i] == background.p[i]){
						refractColor = Point3D();
					}
				}
					resultColor += refractColor*iInfo.material->transparent;
				}else{
					Point3D refractColor = Point3D(1,1,1);
				}
			}
			
		}
		
	}
	/**Clamp the pixel values*/
	for(int i=0; i<3; i++){
		if(resultColor.p[i] > 1.0){
			resultColor.p[i] = 1.0;
		}else if(resultColor.p[i] < 0){
			resultColor.p[i] = 0;
		}
	}
	
	return resultColor;
	
}

//////////////////
// OpenGL stuff //
//////////////////
void RayMaterial::drawOpenGL(void){
}
void RayTexture::setUpOpenGL(void){
}
