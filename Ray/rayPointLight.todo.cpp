#include <math.h>
#include <GL/glut.h>
#include "rayPointLight.h"
#include "rayScene.h"

////////////////////////
//  Ray-tracing stuff //
////////////////////////
double clampPL(double input){
	if(input < 0){
		input = 0;
	}else if(input > 1){
		input = 1;
	}
	return input;
}

Point3D RayPointLight::getDiffuse(Point3D cameraPosition,RayIntersectionInfo& iInfo){
	double distance, attenuation;
	Point3D direct, diffuse, norm;
	norm = iInfo.normal;
	direct = (location - iInfo.iCoordinate);
	distance = direct.length();
	direct/=direct.length();
	/**Attenuation function*/
	attenuation = constAtten + linearAtten*distance + quadAtten*pow(distance,2);
	diffuse = color*norm.dot(direct)/attenuation;

	return diffuse;
}

Point3D RayPointLight::getSpecular(Point3D cameraPosition,RayIntersectionInfo& iInfo){
    Point3D direct = (location - iInfo.iCoordinate);
    double distance = direct.length();
	direct/=direct.length();
    double attenuation = constAtten + linearAtten*distance + quadAtten*pow(distance,2);
	Point3D norm = iInfo.normal/iInfo.normal.length();
	Point3D pointL = color*norm.dot(direct)/ attenuation;
	/**Determine whether light direction is the same with normal direction*/
	double normDir = norm.dot(direct);
    if(normDir<0){
		normDir = 0;
	}
    if(normDir>1){
		normDir = 1;
	}
	/**Calculate the specular vector direction*/
    Point3D speDir = (direct - (direct*2-norm*normDir*2));
	speDir/=speDir.length();
	/**Calculate the camera position direction*/
    Point3D camDir = (iInfo.iCoordinate - cameraPosition);
	camDir/=camDir.length();
   /**Determine whether camera vector is the same with specular vector*/
    double speCam = camDir.dot(speDir);
    if(speCam < 0){
		speCam = 0;
	}
    if(speCam > 1){
		speCam = 1;
	}
   
    Point3D specular = pointL*pow(speCam,iInfo.material->specularFallOff)*iInfo.material->specular;
   
    return specular;
}
int RayPointLight::isInShadow(RayIntersectionInfo& iInfo,RayShape* shape,int& isectCount){
		double interNum;
        Point3D d = -(location - iInfo.iCoordinate);
		Point3D P = iInfo.iCoordinate;
		RayIntersectionInfo tempiInfo = iInfo;
        Ray3D shadowBeam = Ray3D(P, d);

		/**Calculate the position of shadow beam, which is the inversed direction from the origion*/
        shadowBeam.position = shadowBeam(RAYEPS);

		/**Calculate the intesection nums*/
        interNum = shape->intersect(shadowBeam, tempiInfo, -1);
        Point3D N = tempiInfo.normal;

		if (d.dot(N)>= 0) {
             return 1;
        }
		/**If the shadow bean doesn't intersect with object, there's no shadow*/
		if (interNum > 0){
           return 0;		
		}
		return 1;
}
Point3D RayPointLight::transparency(RayIntersectionInfo& iInfo,RayShape* shape,Point3D cLimit){
		double dist = 0.0;
		Point3D key;
		Point3D direction = (location - iInfo.iCoordinate);
        RayIntersectionInfo tempInfo = iInfo;
		Point3D tempLimit = cLimit;
        Point3D transparency=Point3D(1, 1, 1);
		Ray3D shadow = Ray3D(iInfo.iCoordinate, -direction);
		shadow.position = shadow(RAYEPS);
        
		while ((key[0] < tempLimit[0]) && (key[1] < tempLimit[1]) && (key[2] < tempLimit[2])) {
           dist = shape->intersect(shadow, tempInfo, -1);
		   /**If ray doesn't intersect with shape or the side faces to camera, return default*/
		   if (dist == -1 || (-direction).dot(tempInfo.normal) >= 0) {
               return transparency;
           }
           key = tempInfo.material->transparent;
		   transparency = transparency - key;
           tempLimit /= key;
        }

		clampPL(transparency[0]);
		clampPL(transparency[1]);
		clampPL(transparency[2]);

        return transparency;
}


//////////////////
// OpenGL stuff //
//////////////////
void RayPointLight::drawOpenGL(int index){
}