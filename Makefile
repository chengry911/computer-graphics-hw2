TARGET  = Assignment2
SRCS	= 	Util/time \
		Util/cmdLineParser \
		Util/geometry \
		Util/geometry.todo \
		Image/bmp \
		Image/image \
		Image/image.todo \
		Image/jpeg \
		Image/lineSegments \
		Image/lineSegments.todo \
		Ray/mouse \
		Ray/rayBox \
		Ray/rayBox.todo \
		Ray/rayCamera \
		Ray/rayCamera.todo \
		Ray/rayCone \
		Ray/rayCone.todo \
		Ray/rayCylinder \
		Ray/rayCylinder.todo \
		Ray/rayDirectionalLight \
		Ray/rayDirectionalLight.todo \
		Ray/rayFileInstance \
		Ray/rayGroup \
		Ray/rayGroup.todo \
		Ray/rayKey \
		Ray/rayPointLight \
		Ray/rayPointLight.todo \
		Ray/rayScene \
		Ray/rayScene.todo \
		Ray/raySphere \
		Ray/raySphere.todo \
		Ray/raySpotLight \
		Ray/raySpotLight.todo \
		Ray/rayTriangle \
		Ray/rayTriangle.todo \
		Ray/rayWindow \
		main

CC   = g++
CFLAGS		+= -I.
LFLAGS		+= -LJPEG -lJPEG -lglut -lGLU -lGL

OBJECTS		= ${addsuffix .o, ${SRCS}}
CLEAN		= *.o .depend ${TARGET} *.dsp *.dsw *.bak

#############################################################
all: debug

debug: CFLAGS += -DDEBUG -g3
debug: ${TARGET}

release: CFLAGS += -O2 -DRELEASE -pipe -fomit-frame-pointer
release: ${TARGET}

${TARGET}: depend JPEG/libJPEG.a ${OBJECTS}
	${CC} -o $@ ${OBJECTS} ${LFLAGS}

clean:	
	/bin/rm -f ${CLEAN}

.cpp.o:
	${CC} ${CFLAGS} -o $@ -c $<

JPEG/libJPEG.a:
	${MAKE} -C JPEG

depend: 
	makedepend -- ${CFLAGS} -- ${addsuffix .cpp, ${SRCS}}
# DO NOT DELETE

Util/time.o: /usr/include/stdlib.h /usr/include/features.h
Util/time.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
Util/time.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Util/time.o: /usr/include/sys/timeb.h /usr/include/time.h
Util/time.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
Util/time.o: /usr/include/bits/typesizes.h /usr/include/sys/time.h
Util/time.o: /usr/include/bits/time.h /usr/include/sys/select.h
Util/time.o: /usr/include/bits/select.h /usr/include/bits/sigset.h
Util/cmdLineParser.o: /usr/include/stdio.h /usr/include/features.h
Util/cmdLineParser.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
Util/cmdLineParser.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Util/cmdLineParser.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
Util/cmdLineParser.o: /usr/include/bits/typesizes.h /usr/include/libio.h
Util/cmdLineParser.o: /usr/include/_G_config.h /usr/include/wchar.h
Util/cmdLineParser.o: /usr/include/bits/wchar.h /usr/include/gconv.h
Util/cmdLineParser.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Util/cmdLineParser.o: /usr/include/bits/stdio_lim.h
Util/cmdLineParser.o: /usr/include/bits/sys_errlist.h /usr/include/stdlib.h
Util/cmdLineParser.o: /usr/include/string.h /usr/include/assert.h
Util/cmdLineParser.o: Util/cmdLineParser.h
Util/geometry.o: /usr/include/stdlib.h /usr/include/features.h
Util/geometry.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
Util/geometry.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Util/geometry.o: /usr/include/math.h /usr/include/bits/huge_val.h
Util/geometry.o: /usr/include/bits/mathdef.h /usr/include/bits/mathcalls.h
Util/geometry.o: Util/geometry.h
Util/geometry.todo.o: /usr/include/stdlib.h /usr/include/features.h
Util/geometry.todo.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
Util/geometry.todo.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Util/geometry.todo.o: /usr/include/math.h /usr/include/bits/huge_val.h
Util/geometry.todo.o: /usr/include/bits/mathdef.h
Util/geometry.todo.o: /usr/include/bits/mathcalls.h Util/geometry.h
Image/bmp.o: Image/bmp.h Image/image.h /usr/include/stdio.h
Image/bmp.o: /usr/include/features.h /usr/include/sys/cdefs.h
Image/bmp.o: /usr/include/gnu/stubs.h
Image/bmp.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Image/bmp.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
Image/bmp.o: /usr/include/bits/typesizes.h /usr/include/libio.h
Image/bmp.o: /usr/include/_G_config.h /usr/include/wchar.h
Image/bmp.o: /usr/include/bits/wchar.h /usr/include/gconv.h
Image/bmp.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Image/bmp.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
Image/bmp.o: Image/lineSegments.h /usr/include/stdlib.h
Image/image.o: /usr/include/string.h /usr/include/features.h
Image/image.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
Image/image.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Image/image.o: /usr/include/stdlib.h Image/image.h /usr/include/stdio.h
Image/image.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
Image/image.o: /usr/include/bits/typesizes.h /usr/include/libio.h
Image/image.o: /usr/include/_G_config.h /usr/include/wchar.h
Image/image.o: /usr/include/bits/wchar.h /usr/include/gconv.h
Image/image.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Image/image.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
Image/image.o: Image/lineSegments.h ./Util/cmdLineParser.h ./Image/bmp.h
Image/image.o: ./Image/jpeg.h
Image/image.todo.o: Image/image.h /usr/include/stdio.h
Image/image.todo.o: /usr/include/features.h /usr/include/sys/cdefs.h
Image/image.todo.o: /usr/include/gnu/stubs.h
Image/image.todo.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Image/image.todo.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
Image/image.todo.o: /usr/include/bits/typesizes.h /usr/include/libio.h
Image/image.todo.o: /usr/include/_G_config.h /usr/include/wchar.h
Image/image.todo.o: /usr/include/bits/wchar.h /usr/include/gconv.h
Image/image.todo.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Image/image.todo.o: /usr/include/bits/stdio_lim.h
Image/image.todo.o: /usr/include/bits/sys_errlist.h Image/lineSegments.h
Image/image.todo.o: /usr/include/stdlib.h /usr/include/math.h
Image/image.todo.o: /usr/include/bits/huge_val.h /usr/include/bits/mathdef.h
Image/image.todo.o: /usr/include/bits/mathcalls.h
Image/jpeg.o: Image/jpeg.h Image/image.h /usr/include/stdio.h
Image/jpeg.o: /usr/include/features.h /usr/include/sys/cdefs.h
Image/jpeg.o: /usr/include/gnu/stubs.h
Image/jpeg.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Image/jpeg.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
Image/jpeg.o: /usr/include/bits/typesizes.h /usr/include/libio.h
Image/jpeg.o: /usr/include/_G_config.h /usr/include/wchar.h
Image/jpeg.o: /usr/include/bits/wchar.h /usr/include/gconv.h
Image/jpeg.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Image/jpeg.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
Image/jpeg.o: Image/lineSegments.h /usr/include/assert.h
Image/jpeg.o: /usr/include/stdlib.h ./JPEG/jpeglib.h ./JPEG/jconfig.h
Image/jpeg.o: ./JPEG/jmorecfg.h /usr/include/setjmp.h
Image/jpeg.o: /usr/include/bits/setjmp.h /usr/include/bits/sigset.h
Image/lineSegments.o: Image/lineSegments.h /usr/include/stdio.h
Image/lineSegments.o: /usr/include/features.h /usr/include/sys/cdefs.h
Image/lineSegments.o: /usr/include/gnu/stubs.h
Image/lineSegments.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Image/lineSegments.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
Image/lineSegments.o: /usr/include/bits/typesizes.h /usr/include/libio.h
Image/lineSegments.o: /usr/include/_G_config.h /usr/include/wchar.h
Image/lineSegments.o: /usr/include/bits/wchar.h /usr/include/gconv.h
Image/lineSegments.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Image/lineSegments.o: /usr/include/bits/stdio_lim.h
Image/lineSegments.o: /usr/include/bits/sys_errlist.h /usr/include/math.h
Image/lineSegments.o: /usr/include/bits/huge_val.h
Image/lineSegments.o: /usr/include/bits/mathdef.h
Image/lineSegments.o: /usr/include/bits/mathcalls.h
Image/lineSegments.todo.o: Image/lineSegments.h /usr/include/stdio.h
Image/lineSegments.todo.o: /usr/include/features.h /usr/include/sys/cdefs.h
Image/lineSegments.todo.o: /usr/include/gnu/stubs.h
Image/lineSegments.todo.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Image/lineSegments.todo.o: /usr/include/bits/types.h
Image/lineSegments.todo.o: /usr/include/bits/wordsize.h
Image/lineSegments.todo.o: /usr/include/bits/typesizes.h /usr/include/libio.h
Image/lineSegments.todo.o: /usr/include/_G_config.h /usr/include/wchar.h
Image/lineSegments.todo.o: /usr/include/bits/wchar.h /usr/include/gconv.h
Image/lineSegments.todo.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Image/lineSegments.todo.o: /usr/include/bits/stdio_lim.h
Image/lineSegments.todo.o: /usr/include/bits/sys_errlist.h
Image/lineSegments.todo.o: /usr/include/math.h /usr/include/bits/huge_val.h
Image/lineSegments.todo.o: /usr/include/bits/mathdef.h
Image/lineSegments.todo.o: /usr/include/bits/mathcalls.h
Ray/mouse.o: Ray/mouse.h /usr/include/GL/glut.h
Ray/mouse.o: /usr/include/GL/freeglut_std.h /usr/include/GL/gl.h
Ray/mouse.o: /usr/include/GL/glext.h
Ray/mouse.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Ray/mouse.o: /usr/include/GL/glu.h ./Util/geometry.h
Ray/rayBox.o: /usr/include/stdio.h /usr/include/features.h
Ray/rayBox.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
Ray/rayBox.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Ray/rayBox.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
Ray/rayBox.o: /usr/include/bits/typesizes.h /usr/include/libio.h
Ray/rayBox.o: /usr/include/_G_config.h /usr/include/wchar.h
Ray/rayBox.o: /usr/include/bits/wchar.h /usr/include/gconv.h
Ray/rayBox.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Ray/rayBox.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
Ray/rayBox.o: /usr/include/stdlib.h /usr/include/math.h
Ray/rayBox.o: /usr/include/bits/huge_val.h /usr/include/bits/mathdef.h
Ray/rayBox.o: /usr/include/bits/mathcalls.h Ray/rayBox.h ./Util/geometry.h
Ray/rayBox.o: Ray/rayShape.h Ray/rayScene.h ./Image/image.h
Ray/rayBox.o: Image/lineSegments.h /usr/include/GL/glut.h
Ray/rayBox.o: /usr/include/GL/freeglut_std.h /usr/include/GL/gl.h
Ray/rayBox.o: /usr/include/GL/glext.h /usr/include/GL/glu.h Ray/rayLight.h
Ray/rayBox.o: Ray/rayGroup.h Ray/rayKey.h ./Util/parameterSamples.h
Ray/rayBox.o: ./Util/parameterSamples.todo.inl Ray/rayCamera.h
Ray/rayBox.todo.o: /usr/include/math.h /usr/include/features.h
Ray/rayBox.todo.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
Ray/rayBox.todo.o: /usr/include/bits/huge_val.h /usr/include/bits/mathdef.h
Ray/rayBox.todo.o: /usr/include/bits/mathcalls.h /usr/include/GL/glut.h
Ray/rayBox.todo.o: /usr/include/GL/freeglut_std.h /usr/include/GL/gl.h
Ray/rayBox.todo.o: /usr/include/GL/glext.h
Ray/rayBox.todo.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Ray/rayBox.todo.o: /usr/include/GL/glu.h Ray/rayScene.h ./Util/geometry.h
Ray/rayBox.todo.o: ./Image/image.h /usr/include/stdio.h
Ray/rayBox.todo.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
Ray/rayBox.todo.o: /usr/include/bits/typesizes.h /usr/include/libio.h
Ray/rayBox.todo.o: /usr/include/_G_config.h /usr/include/wchar.h
Ray/rayBox.todo.o: /usr/include/bits/wchar.h /usr/include/gconv.h
Ray/rayBox.todo.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Ray/rayBox.todo.o: /usr/include/bits/stdio_lim.h
Ray/rayBox.todo.o: /usr/include/bits/sys_errlist.h Image/lineSegments.h
Ray/rayBox.todo.o: Ray/rayShape.h Ray/rayLight.h Ray/rayGroup.h Ray/rayKey.h
Ray/rayBox.todo.o: ./Util/parameterSamples.h ./Util/parameterSamples.todo.inl
Ray/rayBox.todo.o: Ray/rayCamera.h Ray/rayBox.h
Ray/rayCamera.o: Ray/rayCamera.h /usr/include/stdio.h /usr/include/features.h
Ray/rayCamera.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
Ray/rayCamera.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Ray/rayCamera.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
Ray/rayCamera.o: /usr/include/bits/typesizes.h /usr/include/libio.h
Ray/rayCamera.o: /usr/include/_G_config.h /usr/include/wchar.h
Ray/rayCamera.o: /usr/include/bits/wchar.h /usr/include/gconv.h
Ray/rayCamera.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Ray/rayCamera.o: /usr/include/bits/stdio_lim.h
Ray/rayCamera.o: /usr/include/bits/sys_errlist.h ./Util/geometry.h
Ray/rayCamera.todo.o: /usr/include/GL/glut.h /usr/include/GL/freeglut_std.h
Ray/rayCamera.todo.o: /usr/include/GL/gl.h /usr/include/GL/glext.h
Ray/rayCamera.todo.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Ray/rayCamera.todo.o: /usr/include/GL/glu.h /usr/include/math.h
Ray/rayCamera.todo.o: /usr/include/features.h /usr/include/sys/cdefs.h
Ray/rayCamera.todo.o: /usr/include/gnu/stubs.h /usr/include/bits/huge_val.h
Ray/rayCamera.todo.o: /usr/include/bits/mathdef.h
Ray/rayCamera.todo.o: /usr/include/bits/mathcalls.h Ray/rayCamera.h
Ray/rayCamera.todo.o: /usr/include/stdio.h /usr/include/bits/types.h
Ray/rayCamera.todo.o: /usr/include/bits/wordsize.h
Ray/rayCamera.todo.o: /usr/include/bits/typesizes.h /usr/include/libio.h
Ray/rayCamera.todo.o: /usr/include/_G_config.h /usr/include/wchar.h
Ray/rayCamera.todo.o: /usr/include/bits/wchar.h /usr/include/gconv.h
Ray/rayCamera.todo.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Ray/rayCamera.todo.o: /usr/include/bits/stdio_lim.h
Ray/rayCamera.todo.o: /usr/include/bits/sys_errlist.h ./Util/geometry.h
Ray/rayCone.o: /usr/include/stdio.h /usr/include/features.h
Ray/rayCone.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
Ray/rayCone.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Ray/rayCone.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
Ray/rayCone.o: /usr/include/bits/typesizes.h /usr/include/libio.h
Ray/rayCone.o: /usr/include/_G_config.h /usr/include/wchar.h
Ray/rayCone.o: /usr/include/bits/wchar.h /usr/include/gconv.h
Ray/rayCone.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Ray/rayCone.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
Ray/rayCone.o: /usr/include/stdlib.h /usr/include/math.h
Ray/rayCone.o: /usr/include/bits/huge_val.h /usr/include/bits/mathdef.h
Ray/rayCone.o: /usr/include/bits/mathcalls.h Ray/rayCone.h ./Util/geometry.h
Ray/rayCone.o: Ray/rayShape.h Ray/rayScene.h ./Image/image.h
Ray/rayCone.o: Image/lineSegments.h /usr/include/GL/glut.h
Ray/rayCone.o: /usr/include/GL/freeglut_std.h /usr/include/GL/gl.h
Ray/rayCone.o: /usr/include/GL/glext.h /usr/include/GL/glu.h Ray/rayLight.h
Ray/rayCone.o: Ray/rayGroup.h Ray/rayKey.h ./Util/parameterSamples.h
Ray/rayCone.o: ./Util/parameterSamples.todo.inl Ray/rayCamera.h
Ray/rayCone.todo.o: /usr/include/math.h /usr/include/features.h
Ray/rayCone.todo.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
Ray/rayCone.todo.o: /usr/include/bits/huge_val.h /usr/include/bits/mathdef.h
Ray/rayCone.todo.o: /usr/include/bits/mathcalls.h /usr/include/GL/glut.h
Ray/rayCone.todo.o: /usr/include/GL/freeglut_std.h /usr/include/GL/gl.h
Ray/rayCone.todo.o: /usr/include/GL/glext.h
Ray/rayCone.todo.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Ray/rayCone.todo.o: /usr/include/GL/glu.h Ray/rayScene.h ./Util/geometry.h
Ray/rayCone.todo.o: ./Image/image.h /usr/include/stdio.h
Ray/rayCone.todo.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
Ray/rayCone.todo.o: /usr/include/bits/typesizes.h /usr/include/libio.h
Ray/rayCone.todo.o: /usr/include/_G_config.h /usr/include/wchar.h
Ray/rayCone.todo.o: /usr/include/bits/wchar.h /usr/include/gconv.h
Ray/rayCone.todo.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Ray/rayCone.todo.o: /usr/include/bits/stdio_lim.h
Ray/rayCone.todo.o: /usr/include/bits/sys_errlist.h Image/lineSegments.h
Ray/rayCone.todo.o: Ray/rayShape.h Ray/rayLight.h Ray/rayGroup.h Ray/rayKey.h
Ray/rayCone.todo.o: ./Util/parameterSamples.h
Ray/rayCone.todo.o: ./Util/parameterSamples.todo.inl Ray/rayCamera.h
Ray/rayCone.todo.o: Ray/rayCone.h
Ray/rayCylinder.o: /usr/include/stdio.h /usr/include/features.h
Ray/rayCylinder.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
Ray/rayCylinder.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Ray/rayCylinder.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
Ray/rayCylinder.o: /usr/include/bits/typesizes.h /usr/include/libio.h
Ray/rayCylinder.o: /usr/include/_G_config.h /usr/include/wchar.h
Ray/rayCylinder.o: /usr/include/bits/wchar.h /usr/include/gconv.h
Ray/rayCylinder.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Ray/rayCylinder.o: /usr/include/bits/stdio_lim.h
Ray/rayCylinder.o: /usr/include/bits/sys_errlist.h /usr/include/stdlib.h
Ray/rayCylinder.o: /usr/include/math.h /usr/include/bits/huge_val.h
Ray/rayCylinder.o: /usr/include/bits/mathdef.h /usr/include/bits/mathcalls.h
Ray/rayCylinder.o: Ray/rayCylinder.h ./Util/geometry.h Ray/rayShape.h
Ray/rayCylinder.o: Ray/rayScene.h ./Image/image.h Image/lineSegments.h
Ray/rayCylinder.o: /usr/include/GL/glut.h /usr/include/GL/freeglut_std.h
Ray/rayCylinder.o: /usr/include/GL/gl.h /usr/include/GL/glext.h
Ray/rayCylinder.o: /usr/include/GL/glu.h Ray/rayLight.h Ray/rayGroup.h
Ray/rayCylinder.o: Ray/rayKey.h ./Util/parameterSamples.h
Ray/rayCylinder.o: ./Util/parameterSamples.todo.inl Ray/rayCamera.h
Ray/rayCylinder.todo.o: /usr/include/math.h /usr/include/features.h
Ray/rayCylinder.todo.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
Ray/rayCylinder.todo.o: /usr/include/bits/huge_val.h
Ray/rayCylinder.todo.o: /usr/include/bits/mathdef.h
Ray/rayCylinder.todo.o: /usr/include/bits/mathcalls.h /usr/include/GL/glut.h
Ray/rayCylinder.todo.o: /usr/include/GL/freeglut_std.h /usr/include/GL/gl.h
Ray/rayCylinder.todo.o: /usr/include/GL/glext.h
Ray/rayCylinder.todo.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Ray/rayCylinder.todo.o: /usr/include/GL/glu.h Ray/rayScene.h
Ray/rayCylinder.todo.o: ./Util/geometry.h ./Image/image.h
Ray/rayCylinder.todo.o: /usr/include/stdio.h /usr/include/bits/types.h
Ray/rayCylinder.todo.o: /usr/include/bits/wordsize.h
Ray/rayCylinder.todo.o: /usr/include/bits/typesizes.h /usr/include/libio.h
Ray/rayCylinder.todo.o: /usr/include/_G_config.h /usr/include/wchar.h
Ray/rayCylinder.todo.o: /usr/include/bits/wchar.h /usr/include/gconv.h
Ray/rayCylinder.todo.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Ray/rayCylinder.todo.o: /usr/include/bits/stdio_lim.h
Ray/rayCylinder.todo.o: /usr/include/bits/sys_errlist.h Image/lineSegments.h
Ray/rayCylinder.todo.o: Ray/rayShape.h Ray/rayLight.h Ray/rayGroup.h
Ray/rayCylinder.todo.o: Ray/rayKey.h ./Util/parameterSamples.h
Ray/rayCylinder.todo.o: ./Util/parameterSamples.todo.inl Ray/rayCamera.h
Ray/rayCylinder.todo.o: Ray/rayCylinder.h
Ray/rayDirectionalLight.o: /usr/include/stdio.h /usr/include/features.h
Ray/rayDirectionalLight.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
Ray/rayDirectionalLight.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Ray/rayDirectionalLight.o: /usr/include/bits/types.h
Ray/rayDirectionalLight.o: /usr/include/bits/wordsize.h
Ray/rayDirectionalLight.o: /usr/include/bits/typesizes.h /usr/include/libio.h
Ray/rayDirectionalLight.o: /usr/include/_G_config.h /usr/include/wchar.h
Ray/rayDirectionalLight.o: /usr/include/bits/wchar.h /usr/include/gconv.h
Ray/rayDirectionalLight.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Ray/rayDirectionalLight.o: /usr/include/bits/stdio_lim.h
Ray/rayDirectionalLight.o: /usr/include/bits/sys_errlist.h
Ray/rayDirectionalLight.o: /usr/include/string.h /usr/include/math.h
Ray/rayDirectionalLight.o: /usr/include/bits/huge_val.h
Ray/rayDirectionalLight.o: /usr/include/bits/mathdef.h
Ray/rayDirectionalLight.o: /usr/include/bits/mathcalls.h ./Util/geometry.h
Ray/rayDirectionalLight.o: Ray/rayDirectionalLight.h Ray/rayLight.h
Ray/rayDirectionalLight.o: Ray/rayShape.h Ray/rayScene.h ./Image/image.h
Ray/rayDirectionalLight.o: Image/lineSegments.h /usr/include/GL/glut.h
Ray/rayDirectionalLight.o: /usr/include/GL/freeglut_std.h
Ray/rayDirectionalLight.o: /usr/include/GL/gl.h /usr/include/GL/glext.h
Ray/rayDirectionalLight.o: /usr/include/GL/glu.h Ray/rayGroup.h Ray/rayKey.h
Ray/rayDirectionalLight.o: ./Util/parameterSamples.h
Ray/rayDirectionalLight.o: ./Util/parameterSamples.todo.inl Ray/rayCamera.h
Ray/rayDirectionalLight.todo.o: /usr/include/math.h /usr/include/features.h
Ray/rayDirectionalLight.todo.o: /usr/include/sys/cdefs.h
Ray/rayDirectionalLight.todo.o: /usr/include/gnu/stubs.h
Ray/rayDirectionalLight.todo.o: /usr/include/bits/huge_val.h
Ray/rayDirectionalLight.todo.o: /usr/include/bits/mathdef.h
Ray/rayDirectionalLight.todo.o: /usr/include/bits/mathcalls.h
Ray/rayDirectionalLight.todo.o: /usr/include/GL/glut.h
Ray/rayDirectionalLight.todo.o: /usr/include/GL/freeglut_std.h
Ray/rayDirectionalLight.todo.o: /usr/include/GL/gl.h /usr/include/GL/glext.h
Ray/rayDirectionalLight.todo.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Ray/rayDirectionalLight.todo.o: /usr/include/GL/glu.h
Ray/rayDirectionalLight.todo.o: Ray/rayDirectionalLight.h Ray/rayLight.h
Ray/rayDirectionalLight.todo.o: Ray/rayShape.h /usr/include/stdio.h
Ray/rayDirectionalLight.todo.o: /usr/include/bits/types.h
Ray/rayDirectionalLight.todo.o: /usr/include/bits/wordsize.h
Ray/rayDirectionalLight.todo.o: /usr/include/bits/typesizes.h
Ray/rayDirectionalLight.todo.o: /usr/include/libio.h /usr/include/_G_config.h
Ray/rayDirectionalLight.todo.o: /usr/include/wchar.h
Ray/rayDirectionalLight.todo.o: /usr/include/bits/wchar.h
Ray/rayDirectionalLight.todo.o: /usr/include/gconv.h
Ray/rayDirectionalLight.todo.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Ray/rayDirectionalLight.todo.o: /usr/include/bits/stdio_lim.h
Ray/rayDirectionalLight.todo.o: /usr/include/bits/sys_errlist.h
Ray/rayDirectionalLight.todo.o: ./Util/geometry.h Ray/rayScene.h
Ray/rayDirectionalLight.todo.o: ./Image/image.h Image/lineSegments.h
Ray/rayDirectionalLight.todo.o: Ray/rayGroup.h Ray/rayKey.h
Ray/rayDirectionalLight.todo.o: ./Util/parameterSamples.h
Ray/rayDirectionalLight.todo.o: ./Util/parameterSamples.todo.inl
Ray/rayDirectionalLight.todo.o: Ray/rayCamera.h
Ray/rayFileInstance.o: /usr/include/stdio.h /usr/include/features.h
Ray/rayFileInstance.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
Ray/rayFileInstance.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Ray/rayFileInstance.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
Ray/rayFileInstance.o: /usr/include/bits/typesizes.h /usr/include/libio.h
Ray/rayFileInstance.o: /usr/include/_G_config.h /usr/include/wchar.h
Ray/rayFileInstance.o: /usr/include/bits/wchar.h /usr/include/gconv.h
Ray/rayFileInstance.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Ray/rayFileInstance.o: /usr/include/bits/stdio_lim.h
Ray/rayFileInstance.o: /usr/include/bits/sys_errlist.h /usr/include/stdlib.h
Ray/rayFileInstance.o: Ray/rayFileInstance.h ./Util/geometry.h Ray/rayShape.h
Ray/rayFileInstance.o: Ray/rayScene.h ./Image/image.h Image/lineSegments.h
Ray/rayFileInstance.o: /usr/include/GL/glut.h /usr/include/GL/freeglut_std.h
Ray/rayFileInstance.o: /usr/include/GL/gl.h /usr/include/GL/glext.h
Ray/rayFileInstance.o: /usr/include/GL/glu.h Ray/rayLight.h Ray/rayGroup.h
Ray/rayFileInstance.o: Ray/rayKey.h ./Util/parameterSamples.h
Ray/rayFileInstance.o: ./Util/parameterSamples.todo.inl Ray/rayCamera.h
Ray/rayGroup.o: /usr/include/stdio.h /usr/include/features.h
Ray/rayGroup.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
Ray/rayGroup.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Ray/rayGroup.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
Ray/rayGroup.o: /usr/include/bits/typesizes.h /usr/include/libio.h
Ray/rayGroup.o: /usr/include/_G_config.h /usr/include/wchar.h
Ray/rayGroup.o: /usr/include/bits/wchar.h /usr/include/gconv.h
Ray/rayGroup.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Ray/rayGroup.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
Ray/rayGroup.o: /usr/include/assert.h /usr/include/stdlib.h
Ray/rayGroup.o: /usr/include/string.h Ray/rayGroup.h ./Util/geometry.h
Ray/rayGroup.o: Ray/rayShape.h Ray/rayScene.h ./Image/image.h
Ray/rayGroup.o: Image/lineSegments.h /usr/include/GL/glut.h
Ray/rayGroup.o: /usr/include/GL/freeglut_std.h /usr/include/GL/gl.h
Ray/rayGroup.o: /usr/include/GL/glext.h /usr/include/GL/glu.h Ray/rayLight.h
Ray/rayGroup.o: Ray/rayKey.h ./Util/parameterSamples.h
Ray/rayGroup.o: ./Util/parameterSamples.todo.inl Ray/rayCamera.h
Ray/rayGroup.todo.o: /usr/include/stdlib.h /usr/include/features.h
Ray/rayGroup.todo.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
Ray/rayGroup.todo.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Ray/rayGroup.todo.o: /usr/include/GL/glut.h /usr/include/GL/freeglut_std.h
Ray/rayGroup.todo.o: /usr/include/GL/gl.h /usr/include/GL/glext.h
Ray/rayGroup.todo.o: /usr/include/GL/glu.h Ray/rayGroup.h ./Util/geometry.h
Ray/rayGroup.todo.o: Ray/rayShape.h /usr/include/stdio.h
Ray/rayGroup.todo.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
Ray/rayGroup.todo.o: /usr/include/bits/typesizes.h /usr/include/libio.h
Ray/rayGroup.todo.o: /usr/include/_G_config.h /usr/include/wchar.h
Ray/rayGroup.todo.o: /usr/include/bits/wchar.h /usr/include/gconv.h
Ray/rayGroup.todo.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Ray/rayGroup.todo.o: /usr/include/bits/stdio_lim.h
Ray/rayGroup.todo.o: /usr/include/bits/sys_errlist.h Ray/rayScene.h
Ray/rayGroup.todo.o: ./Image/image.h Image/lineSegments.h Ray/rayLight.h
Ray/rayGroup.todo.o: Ray/rayKey.h ./Util/parameterSamples.h
Ray/rayGroup.todo.o: ./Util/parameterSamples.todo.inl Ray/rayCamera.h
Ray/rayKey.o: /usr/include/stdio.h /usr/include/features.h
Ray/rayKey.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
Ray/rayKey.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Ray/rayKey.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
Ray/rayKey.o: /usr/include/bits/typesizes.h /usr/include/libio.h
Ray/rayKey.o: /usr/include/_G_config.h /usr/include/wchar.h
Ray/rayKey.o: /usr/include/bits/wchar.h /usr/include/gconv.h
Ray/rayKey.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Ray/rayKey.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
Ray/rayKey.o: /usr/include/string.h ./Util/geometry.h ./Util/cmdLineParser.h
Ray/rayKey.o: Ray/rayGroup.h Ray/rayShape.h Ray/rayScene.h ./Image/image.h
Ray/rayKey.o: Image/lineSegments.h /usr/include/GL/glut.h
Ray/rayKey.o: /usr/include/GL/freeglut_std.h /usr/include/GL/gl.h
Ray/rayKey.o: /usr/include/GL/glext.h /usr/include/GL/glu.h Ray/rayLight.h
Ray/rayKey.o: Ray/rayKey.h ./Util/parameterSamples.h
Ray/rayKey.o: ./Util/parameterSamples.todo.inl Ray/rayCamera.h
Ray/rayPointLight.o: /usr/include/stdio.h /usr/include/features.h
Ray/rayPointLight.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
Ray/rayPointLight.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Ray/rayPointLight.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
Ray/rayPointLight.o: /usr/include/bits/typesizes.h /usr/include/libio.h
Ray/rayPointLight.o: /usr/include/_G_config.h /usr/include/wchar.h
Ray/rayPointLight.o: /usr/include/bits/wchar.h /usr/include/gconv.h
Ray/rayPointLight.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Ray/rayPointLight.o: /usr/include/bits/stdio_lim.h
Ray/rayPointLight.o: /usr/include/bits/sys_errlist.h /usr/include/string.h
Ray/rayPointLight.o: /usr/include/math.h /usr/include/bits/huge_val.h
Ray/rayPointLight.o: /usr/include/bits/mathdef.h
Ray/rayPointLight.o: /usr/include/bits/mathcalls.h ./Util/geometry.h
Ray/rayPointLight.o: Ray/rayPointLight.h Ray/rayLight.h Ray/rayShape.h
Ray/rayPointLight.o: Ray/rayScene.h ./Image/image.h Image/lineSegments.h
Ray/rayPointLight.o: /usr/include/GL/glut.h /usr/include/GL/freeglut_std.h
Ray/rayPointLight.o: /usr/include/GL/gl.h /usr/include/GL/glext.h
Ray/rayPointLight.o: /usr/include/GL/glu.h Ray/rayGroup.h Ray/rayKey.h
Ray/rayPointLight.o: ./Util/parameterSamples.h
Ray/rayPointLight.o: ./Util/parameterSamples.todo.inl Ray/rayCamera.h
Ray/rayPointLight.todo.o: /usr/include/math.h /usr/include/features.h
Ray/rayPointLight.todo.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
Ray/rayPointLight.todo.o: /usr/include/bits/huge_val.h
Ray/rayPointLight.todo.o: /usr/include/bits/mathdef.h
Ray/rayPointLight.todo.o: /usr/include/bits/mathcalls.h
Ray/rayPointLight.todo.o: /usr/include/GL/glut.h
Ray/rayPointLight.todo.o: /usr/include/GL/freeglut_std.h /usr/include/GL/gl.h
Ray/rayPointLight.todo.o: /usr/include/GL/glext.h
Ray/rayPointLight.todo.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Ray/rayPointLight.todo.o: /usr/include/GL/glu.h Ray/rayPointLight.h
Ray/rayPointLight.todo.o: Ray/rayLight.h Ray/rayShape.h /usr/include/stdio.h
Ray/rayPointLight.todo.o: /usr/include/bits/types.h
Ray/rayPointLight.todo.o: /usr/include/bits/wordsize.h
Ray/rayPointLight.todo.o: /usr/include/bits/typesizes.h /usr/include/libio.h
Ray/rayPointLight.todo.o: /usr/include/_G_config.h /usr/include/wchar.h
Ray/rayPointLight.todo.o: /usr/include/bits/wchar.h /usr/include/gconv.h
Ray/rayPointLight.todo.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Ray/rayPointLight.todo.o: /usr/include/bits/stdio_lim.h
Ray/rayPointLight.todo.o: /usr/include/bits/sys_errlist.h ./Util/geometry.h
Ray/rayPointLight.todo.o: Ray/rayScene.h ./Image/image.h Image/lineSegments.h
Ray/rayPointLight.todo.o: Ray/rayGroup.h Ray/rayKey.h
Ray/rayPointLight.todo.o: ./Util/parameterSamples.h
Ray/rayPointLight.todo.o: ./Util/parameterSamples.todo.inl Ray/rayCamera.h
Ray/rayScene.o: /usr/include/stdlib.h /usr/include/features.h
Ray/rayScene.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
Ray/rayScene.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Ray/rayScene.o: /usr/include/stdio.h /usr/include/bits/types.h
Ray/rayScene.o: /usr/include/bits/wordsize.h /usr/include/bits/typesizes.h
Ray/rayScene.o: /usr/include/libio.h /usr/include/_G_config.h
Ray/rayScene.o: /usr/include/wchar.h /usr/include/bits/wchar.h
Ray/rayScene.o: /usr/include/gconv.h
Ray/rayScene.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Ray/rayScene.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
Ray/rayScene.o: /usr/include/string.h /usr/include/math.h
Ray/rayScene.o: /usr/include/bits/huge_val.h /usr/include/bits/mathdef.h
Ray/rayScene.o: /usr/include/bits/mathcalls.h ./Image/bmp.h Image/image.h
Ray/rayScene.o: Image/lineSegments.h Ray/rayScene.h ./Util/geometry.h
Ray/rayScene.o: ./Image/image.h /usr/include/GL/glut.h
Ray/rayScene.o: /usr/include/GL/freeglut_std.h /usr/include/GL/gl.h
Ray/rayScene.o: /usr/include/GL/glext.h /usr/include/GL/glu.h Ray/rayShape.h
Ray/rayScene.o: Ray/rayLight.h Ray/rayGroup.h Ray/rayKey.h
Ray/rayScene.o: ./Util/parameterSamples.h ./Util/parameterSamples.todo.inl
Ray/rayScene.o: Ray/rayCamera.h Ray/rayPointLight.h Ray/rayDirectionalLight.h
Ray/rayScene.o: Ray/raySpotLight.h Ray/rayFileInstance.h Ray/raySphere.h
Ray/rayScene.o: Ray/rayBox.h Ray/rayCone.h Ray/rayCylinder.h
Ray/rayScene.o: Ray/rayTriangle.h
Ray/rayScene.todo.o: Ray/rayScene.h ./Util/geometry.h ./Image/image.h
Ray/rayScene.todo.o: /usr/include/stdio.h /usr/include/features.h
Ray/rayScene.todo.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
Ray/rayScene.todo.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Ray/rayScene.todo.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
Ray/rayScene.todo.o: /usr/include/bits/typesizes.h /usr/include/libio.h
Ray/rayScene.todo.o: /usr/include/_G_config.h /usr/include/wchar.h
Ray/rayScene.todo.o: /usr/include/bits/wchar.h /usr/include/gconv.h
Ray/rayScene.todo.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Ray/rayScene.todo.o: /usr/include/bits/stdio_lim.h
Ray/rayScene.todo.o: /usr/include/bits/sys_errlist.h Image/lineSegments.h
Ray/rayScene.todo.o: /usr/include/GL/glut.h /usr/include/GL/freeglut_std.h
Ray/rayScene.todo.o: /usr/include/GL/gl.h /usr/include/GL/glext.h
Ray/rayScene.todo.o: /usr/include/GL/glu.h Ray/rayShape.h Ray/rayLight.h
Ray/rayScene.todo.o: Ray/rayGroup.h Ray/rayKey.h ./Util/parameterSamples.h
Ray/rayScene.todo.o: ./Util/parameterSamples.todo.inl Ray/rayCamera.h
Ray/rayScene.todo.o: /usr/include/math.h /usr/include/bits/huge_val.h
Ray/rayScene.todo.o: /usr/include/bits/mathdef.h
Ray/rayScene.todo.o: /usr/include/bits/mathcalls.h
Ray/raySphere.o: /usr/include/stdio.h /usr/include/features.h
Ray/raySphere.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
Ray/raySphere.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Ray/raySphere.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
Ray/raySphere.o: /usr/include/bits/typesizes.h /usr/include/libio.h
Ray/raySphere.o: /usr/include/_G_config.h /usr/include/wchar.h
Ray/raySphere.o: /usr/include/bits/wchar.h /usr/include/gconv.h
Ray/raySphere.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Ray/raySphere.o: /usr/include/bits/stdio_lim.h
Ray/raySphere.o: /usr/include/bits/sys_errlist.h /usr/include/stdlib.h
Ray/raySphere.o: /usr/include/math.h /usr/include/bits/huge_val.h
Ray/raySphere.o: /usr/include/bits/mathdef.h /usr/include/bits/mathcalls.h
Ray/raySphere.o: Ray/raySphere.h ./Util/geometry.h Ray/rayShape.h
Ray/raySphere.o: Ray/rayScene.h ./Image/image.h Image/lineSegments.h
Ray/raySphere.o: /usr/include/GL/glut.h /usr/include/GL/freeglut_std.h
Ray/raySphere.o: /usr/include/GL/gl.h /usr/include/GL/glext.h
Ray/raySphere.o: /usr/include/GL/glu.h Ray/rayLight.h Ray/rayGroup.h
Ray/raySphere.o: Ray/rayKey.h ./Util/parameterSamples.h
Ray/raySphere.o: ./Util/parameterSamples.todo.inl Ray/rayCamera.h
Ray/raySphere.todo.o: /usr/include/math.h /usr/include/features.h
Ray/raySphere.todo.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
Ray/raySphere.todo.o: /usr/include/bits/huge_val.h
Ray/raySphere.todo.o: /usr/include/bits/mathdef.h
Ray/raySphere.todo.o: /usr/include/bits/mathcalls.h /usr/include/GL/glut.h
Ray/raySphere.todo.o: /usr/include/GL/freeglut_std.h /usr/include/GL/gl.h
Ray/raySphere.todo.o: /usr/include/GL/glext.h
Ray/raySphere.todo.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Ray/raySphere.todo.o: /usr/include/GL/glu.h Ray/rayScene.h ./Util/geometry.h
Ray/raySphere.todo.o: ./Image/image.h /usr/include/stdio.h
Ray/raySphere.todo.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
Ray/raySphere.todo.o: /usr/include/bits/typesizes.h /usr/include/libio.h
Ray/raySphere.todo.o: /usr/include/_G_config.h /usr/include/wchar.h
Ray/raySphere.todo.o: /usr/include/bits/wchar.h /usr/include/gconv.h
Ray/raySphere.todo.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Ray/raySphere.todo.o: /usr/include/bits/stdio_lim.h
Ray/raySphere.todo.o: /usr/include/bits/sys_errlist.h Image/lineSegments.h
Ray/raySphere.todo.o: Ray/rayShape.h Ray/rayLight.h Ray/rayGroup.h
Ray/raySphere.todo.o: Ray/rayKey.h ./Util/parameterSamples.h
Ray/raySphere.todo.o: ./Util/parameterSamples.todo.inl Ray/rayCamera.h
Ray/raySphere.todo.o: Ray/raySphere.h
Ray/raySpotLight.o: /usr/include/stdio.h /usr/include/features.h
Ray/raySpotLight.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
Ray/raySpotLight.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Ray/raySpotLight.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
Ray/raySpotLight.o: /usr/include/bits/typesizes.h /usr/include/libio.h
Ray/raySpotLight.o: /usr/include/_G_config.h /usr/include/wchar.h
Ray/raySpotLight.o: /usr/include/bits/wchar.h /usr/include/gconv.h
Ray/raySpotLight.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Ray/raySpotLight.o: /usr/include/bits/stdio_lim.h
Ray/raySpotLight.o: /usr/include/bits/sys_errlist.h /usr/include/string.h
Ray/raySpotLight.o: /usr/include/math.h /usr/include/bits/huge_val.h
Ray/raySpotLight.o: /usr/include/bits/mathdef.h /usr/include/bits/mathcalls.h
Ray/raySpotLight.o: ./Util/geometry.h Ray/raySpotLight.h Ray/rayLight.h
Ray/raySpotLight.o: Ray/rayShape.h Ray/rayScene.h ./Image/image.h
Ray/raySpotLight.o: Image/lineSegments.h /usr/include/GL/glut.h
Ray/raySpotLight.o: /usr/include/GL/freeglut_std.h /usr/include/GL/gl.h
Ray/raySpotLight.o: /usr/include/GL/glext.h /usr/include/GL/glu.h
Ray/raySpotLight.o: Ray/rayGroup.h Ray/rayKey.h ./Util/parameterSamples.h
Ray/raySpotLight.o: ./Util/parameterSamples.todo.inl Ray/rayCamera.h
Ray/raySpotLight.todo.o: /usr/include/math.h /usr/include/features.h
Ray/raySpotLight.todo.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
Ray/raySpotLight.todo.o: /usr/include/bits/huge_val.h
Ray/raySpotLight.todo.o: /usr/include/bits/mathdef.h
Ray/raySpotLight.todo.o: /usr/include/bits/mathcalls.h /usr/include/GL/glut.h
Ray/raySpotLight.todo.o: /usr/include/GL/freeglut_std.h /usr/include/GL/gl.h
Ray/raySpotLight.todo.o: /usr/include/GL/glext.h
Ray/raySpotLight.todo.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Ray/raySpotLight.todo.o: /usr/include/GL/glu.h Ray/rayScene.h
Ray/raySpotLight.todo.o: ./Util/geometry.h ./Image/image.h
Ray/raySpotLight.todo.o: /usr/include/stdio.h /usr/include/bits/types.h
Ray/raySpotLight.todo.o: /usr/include/bits/wordsize.h
Ray/raySpotLight.todo.o: /usr/include/bits/typesizes.h /usr/include/libio.h
Ray/raySpotLight.todo.o: /usr/include/_G_config.h /usr/include/wchar.h
Ray/raySpotLight.todo.o: /usr/include/bits/wchar.h /usr/include/gconv.h
Ray/raySpotLight.todo.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Ray/raySpotLight.todo.o: /usr/include/bits/stdio_lim.h
Ray/raySpotLight.todo.o: /usr/include/bits/sys_errlist.h Image/lineSegments.h
Ray/raySpotLight.todo.o: Ray/rayShape.h Ray/rayLight.h Ray/rayGroup.h
Ray/raySpotLight.todo.o: Ray/rayKey.h ./Util/parameterSamples.h
Ray/raySpotLight.todo.o: ./Util/parameterSamples.todo.inl Ray/rayCamera.h
Ray/raySpotLight.todo.o: Ray/raySpotLight.h
Ray/rayTriangle.o: /usr/include/stdio.h /usr/include/features.h
Ray/rayTriangle.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
Ray/rayTriangle.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Ray/rayTriangle.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
Ray/rayTriangle.o: /usr/include/bits/typesizes.h /usr/include/libio.h
Ray/rayTriangle.o: /usr/include/_G_config.h /usr/include/wchar.h
Ray/rayTriangle.o: /usr/include/bits/wchar.h /usr/include/gconv.h
Ray/rayTriangle.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Ray/rayTriangle.o: /usr/include/bits/stdio_lim.h
Ray/rayTriangle.o: /usr/include/bits/sys_errlist.h /usr/include/stdlib.h
Ray/rayTriangle.o: /usr/include/math.h /usr/include/bits/huge_val.h
Ray/rayTriangle.o: /usr/include/bits/mathdef.h /usr/include/bits/mathcalls.h
Ray/rayTriangle.o: Ray/rayTriangle.h ./Util/geometry.h Ray/rayShape.h
Ray/rayTriangle.o: Ray/rayScene.h ./Image/image.h Image/lineSegments.h
Ray/rayTriangle.o: /usr/include/GL/glut.h /usr/include/GL/freeglut_std.h
Ray/rayTriangle.o: /usr/include/GL/gl.h /usr/include/GL/glext.h
Ray/rayTriangle.o: /usr/include/GL/glu.h Ray/rayLight.h Ray/rayGroup.h
Ray/rayTriangle.o: Ray/rayKey.h ./Util/parameterSamples.h
Ray/rayTriangle.o: ./Util/parameterSamples.todo.inl Ray/rayCamera.h
Ray/rayTriangle.todo.o: /usr/include/stdio.h /usr/include/features.h
Ray/rayTriangle.todo.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
Ray/rayTriangle.todo.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Ray/rayTriangle.todo.o: /usr/include/bits/types.h
Ray/rayTriangle.todo.o: /usr/include/bits/wordsize.h
Ray/rayTriangle.todo.o: /usr/include/bits/typesizes.h /usr/include/libio.h
Ray/rayTriangle.todo.o: /usr/include/_G_config.h /usr/include/wchar.h
Ray/rayTriangle.todo.o: /usr/include/bits/wchar.h /usr/include/gconv.h
Ray/rayTriangle.todo.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Ray/rayTriangle.todo.o: /usr/include/bits/stdio_lim.h
Ray/rayTriangle.todo.o: /usr/include/bits/sys_errlist.h /usr/include/stdlib.h
Ray/rayTriangle.todo.o: /usr/include/math.h /usr/include/bits/huge_val.h
Ray/rayTriangle.todo.o: /usr/include/bits/mathdef.h
Ray/rayTriangle.todo.o: /usr/include/bits/mathcalls.h Ray/rayTriangle.h
Ray/rayTriangle.todo.o: ./Util/geometry.h Ray/rayShape.h Ray/rayScene.h
Ray/rayTriangle.todo.o: ./Image/image.h Image/lineSegments.h
Ray/rayTriangle.todo.o: /usr/include/GL/glut.h /usr/include/GL/freeglut_std.h
Ray/rayTriangle.todo.o: /usr/include/GL/gl.h /usr/include/GL/glext.h
Ray/rayTriangle.todo.o: /usr/include/GL/glu.h Ray/rayLight.h Ray/rayGroup.h
Ray/rayTriangle.todo.o: Ray/rayKey.h ./Util/parameterSamples.h
Ray/rayTriangle.todo.o: ./Util/parameterSamples.todo.inl Ray/rayCamera.h
Ray/rayWindow.o: /usr/include/string.h /usr/include/features.h
Ray/rayWindow.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
Ray/rayWindow.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
Ray/rayWindow.o: ./Util/time.h Ray/rayWindow.h ./Ray/mouse.h ./Ray/rayScene.h
Ray/rayWindow.o: ./Util/geometry.h ./Image/image.h /usr/include/stdio.h
Ray/rayWindow.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
Ray/rayWindow.o: /usr/include/bits/typesizes.h /usr/include/libio.h
Ray/rayWindow.o: /usr/include/_G_config.h /usr/include/wchar.h
Ray/rayWindow.o: /usr/include/bits/wchar.h /usr/include/gconv.h
Ray/rayWindow.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
Ray/rayWindow.o: /usr/include/bits/stdio_lim.h
Ray/rayWindow.o: /usr/include/bits/sys_errlist.h Image/lineSegments.h
Ray/rayWindow.o: /usr/include/GL/glut.h /usr/include/GL/freeglut_std.h
Ray/rayWindow.o: /usr/include/GL/gl.h /usr/include/GL/glext.h
Ray/rayWindow.o: /usr/include/GL/glu.h Ray/rayShape.h Ray/rayLight.h
Ray/rayWindow.o: Ray/rayGroup.h Ray/rayScene.h Ray/rayKey.h
Ray/rayWindow.o: ./Util/parameterSamples.h ./Util/parameterSamples.todo.inl
Ray/rayWindow.o: Ray/rayCamera.h /usr/include/stdlib.h
main.o: /usr/include/stdio.h /usr/include/features.h /usr/include/sys/cdefs.h
main.o: /usr/include/gnu/stubs.h
main.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stddef.h
main.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
main.o: /usr/include/bits/typesizes.h /usr/include/libio.h
main.o: /usr/include/_G_config.h /usr/include/wchar.h
main.o: /usr/include/bits/wchar.h /usr/include/gconv.h
main.o: /usr/lib/gcc/i386-redhat-linux/3.4.4/include/stdarg.h
main.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
main.o: /usr/include/stdlib.h ./Util/cmdLineParser.h /usr/include/string.h
main.o: ./Ray/rayScene.h ./Util/geometry.h ./Image/image.h
main.o: Image/lineSegments.h /usr/include/GL/glut.h
main.o: /usr/include/GL/freeglut_std.h /usr/include/GL/gl.h
main.o: /usr/include/GL/glext.h /usr/include/GL/glu.h Ray/rayShape.h
main.o: Ray/rayLight.h Ray/rayGroup.h Ray/rayScene.h Ray/rayKey.h
main.o: ./Util/parameterSamples.h ./Util/parameterSamples.todo.inl
main.o: Ray/rayCamera.h ./Util/time.h
